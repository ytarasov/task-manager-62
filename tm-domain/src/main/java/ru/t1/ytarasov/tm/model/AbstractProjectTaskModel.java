package ru.t1.ytarasov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import ru.t1.ytarasov.tm.enumerated.Status;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;
import java.util.Date;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
public abstract class AbstractProjectTaskModel extends AbstractUserOwnedModel {

    public AbstractProjectTaskModel(
            @NotNull final User user,
            @NotNull final String name,
            @NotNull final String description
    ) {
        this.user = user;
        this.name = name;
        this.description = description;
    }

    public AbstractProjectTaskModel(@NotNull String name, @NotNull String description) {
        this.name = name;
        this.description = description;
    }

    @NotNull
    @Column(nullable = false)
    protected String name = "";

    @Column
    @NotNull
    protected String description = "";

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    protected Status status = Status.NOT_STARTED;

    @Override
    public String toString() {
        return getId()+ " " + name + ": " + description + ": " + Status.toName(status);
    }

}

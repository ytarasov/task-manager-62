package ru.t1.ytarasov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.ytarasov.tm.dto.request.project.ProjectRemoveByIdRequest;
import ru.t1.ytarasov.tm.event.ConsoleEvent;
import ru.t1.ytarasov.tm.util.TerminalUtil;

@Component
public final class ProjectRemoveByIdListener extends AbstractProjectListener {

    @NotNull
    public static final String NAME = "project-remove-by-id";

    @NotNull
    public static final String DESCRIPTION = "Remove project by id";

    @Override
    @EventListener(condition = "@projectRemoveByIdListener.getName() == #event.name")
    public void handler(@NotNull ConsoleEvent event) throws Exception {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final @Nullable String id = TerminalUtil.nextLine();
        @NotNull final ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(getToken());
        request.setProjectId(id);
        getProjectEndpoint().removeProjectById(request);
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

}

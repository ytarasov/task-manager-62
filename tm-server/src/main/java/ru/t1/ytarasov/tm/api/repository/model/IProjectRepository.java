package ru.t1.ytarasov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.ytarasov.tm.model.Project;

import java.util.List;

@Repository
public interface IProjectRepository extends IUserOwnedRepository<Project> {

    @Modifying
    @Transactional
    void deleteByUserId(@NotNull final String userId);

    @Modifying
    @Transactional
    void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    @Nullable
    @Query("SELECT p FROM Project p ORDER BY :sortOrder")
    List<Project> findAllWithSort(@NotNull @Param("sortOrder") final String sortOrder);

    @Nullable
    @Query("SELECT p FROM Project p WHERE p.user.id = :userId ORDER BY :sortOrder")
    List<Project> findAllWithUserIdAndSort(
            @NotNull @Param("userId") final String userId,
            @NotNull @Param("sortOrder") final String sortOrder
    );

    @Nullable
    List<Project> findByUserId(@NotNull final String userId);

    @Nullable
    Project findByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    Long countByUserId(@NotNull final String userId);

    Boolean existsByUserIdAndId(@NotNull final String userId, @NotNull final String id);

}

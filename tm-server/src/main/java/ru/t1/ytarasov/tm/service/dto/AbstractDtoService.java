package ru.t1.ytarasov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.t1.ytarasov.tm.api.service.dto.IDtoService;
import ru.t1.ytarasov.tm.dto.model.AbstractModelDTO;
import ru.t1.ytarasov.tm.enumerated.Sort;

@Service
public abstract class AbstractDtoService<M extends AbstractModelDTO> implements IDtoService<M> {

    protected String getSortType(@NotNull final Sort sort) {
        if (sort == Sort.BY_NAME) return "name";
        else if (sort == Sort.BY_STATUS) return "status";
        else return "created";
    }

}

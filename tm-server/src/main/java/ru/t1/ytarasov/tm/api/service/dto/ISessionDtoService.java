package ru.t1.ytarasov.tm.api.service.dto;

import ru.t1.ytarasov.tm.dto.model.SessionDTO;

public interface ISessionDtoService extends IUserOwnedDtoService<SessionDTO> {
}

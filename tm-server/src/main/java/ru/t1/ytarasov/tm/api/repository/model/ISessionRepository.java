package ru.t1.ytarasov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.ytarasov.tm.model.Session;

import java.util.List;

@Repository
public interface ISessionRepository extends IUserOwnedRepository<Session> {

    @Modifying
    @Transactional
    void deleteByUserId(@NotNull final String userId);

    @Modifying
    @Transactional
    void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    @Nullable
    List<Session> findByUserId(@NotNull final String userId);

    @Nullable
    Session findOByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    Long countByUserId(@NotNull final String userId);

    Boolean existsByUserIdAndId (@NotNull final String userId, @NotNull final String id);

}
